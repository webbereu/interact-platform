package com.interact.core.entitlements.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.interact.core.entitlements.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
	Role findByName(String name);
}
