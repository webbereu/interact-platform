var directives = angular.module('briskAnalyticsDirectives', []);

directives.directive('justGage', ['$timeout', function ($timeout) {
      return {
        restrict: 'E',
        scope: {
          id: '@',
          min: '=',
          max: '=',
          title: '@',
          value: '=',
          style: '@'
        },
        template: '<div id={{id}}-justgage style={{style}}></div>',
        link: function (scope) {
          $timeout(function () {
            scope.g = new JustGage({
              id: scope.id + '-justgage',
              min: scope.min,
              max: scope.max,
              title: scope.title,
              value: scope.value,
              style: scope.style
            });

            scope.$watch('value', function (updatedValue) {
                if (updatedValue) {
                  scope.g.refresh(updatedValue);
                }
              }, true);
            scope.$watch('min', function (updatedValue) {
                if (updatedValue) {
                	scope.g.refresh(updatedValue);
                }
              }, true);
            scope.$watch('max', function (updatedValue) {
                if (updatedValue) {
                	scope.g.refresh(updatedValue);
                }
              }, true);
          });
        }
      };
    }]);
