﻿var app = angular.module('interactApp', ['ngRoute', 'interactControllers']);

//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider) {
    $routeProvider
    	.when('/',
            {
                controller: 'HomeController',
                templateUrl: 'app/views/home.html'
            })
    	.when('/ads',
            {
                controller: 'AdsController',
                templateUrl: 'app/views/ads/ads.html'
            })
    	.when('/ads/viewAll',
            {
                controller: 'AdsController',
                templateUrl: 'app/views/ads/ads.html'
            })
    	.when('/ads/reports',
            {
                controller: 'AdsController',
                templateUrl: 'app/views/ads/adReports.html'
            })
    	.when('/apps',
            {
                controller: 'AppsController',
                templateUrl: 'app/views/apps/apps.html'
            })
    	.when('/apps/viewAll',
            {
                controller: 'AppsController',
                templateUrl: 'app/views/apps/apps.html'
            })
    	.when('/apps/categories',
            {
                controller: 'AppsController',
                templateUrl: 'app/views/apps/appCategories.html'
            })
    	.when('/survey',
            {
                controller: 'SurveyController',
                templateUrl: 'app/views/survey/survey.html'
            })
    	.when('/survey/categories',
            {
                controller: 'SurveyController',
                templateUrl: 'app/views/survey/surveyCategories.html'
            })
    	.when('/survey/reports',
            {
                controller: 'SurveyController',
                templateUrl: 'app/views/survey/surveyReports.html'
            })
    	.when('/survey/manage',
            {
                controller: 'SurveyController',
                templateUrl: 'app/views/survey/surveyManage.html'
            })
    	.when('/settings',
            {
                controller: 'SettingsController',
                templateUrl: 'app/views/settings/settings.html'
            })
    	.when('/settings/positions',
            {
                controller: 'SettingsController',
                templateUrl: 'app/views/settings/positionSettings.html'
            })
    	.when('/settings/devices',
            {
                controller: 'SettingsController',
                templateUrl: 'app/views/settings/deviceSettings.html'
            })
    	.when('/settings/general',
            {
                controller: 'SettingsController',
                templateUrl: 'app/views/settings/generalSettings.html'
            })
        .otherwise({ redirectTo: '/' });
});




