package com.interact.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "com.interact" })
public class ComponentConfig {
	//nothing here
}